function Send() {

    var values = $('#api-form').serialize();
    $.ajax({
        url: "/action.php",
        type: "post",
        data: values,
        success: function (response) {
            $('#result').html('');
            $('#result').prepend(response);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
        }
    });
}
