<? //php// include_once 'lib/Mysql.php'; ?>
<? //php// include_once 'lib/Action.php'; ?>

<? //php $Action = new Action(); ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Test</title>
    <link href="/lib/css/bootstrap.css" rel="stylesheet">

    <script src="/lib/js/jquery.js"></script>
    <script src="/lib/js/bootstrap.min.js"></script>
    <script src="/lib/js/jquery.validate.min.js"></script>
    <script src="/lib/js/custom.js"></script>


</head>
<body>

<div class="row" style="margin-top: 200px">
    <div class="col-md-3 col-lg-offset-4 well">

        <form id="api-form" method="post" action="/action.php">
            <div class="form-group">
                <label>Please select an action</label>
                <select name="request" class="form-control">
                    <option value="region/">Получение списка кодов регионов</option>
                    <option value="locality_search/?term=Екатеринбург&region_id=&region=66/">Поиск города</option>
                    <option value='create/{"id": 705043,"name": "Иван","surname": "иванов","patronymic": "Иванович","passport": "0000000003","passport_code": "123-321","passport_date": "1999-01-01","mobile_phone": "9000000000","email": "ivan@ivanov.ru","scoring": "Хороший","locality_name": "Москва","locality": 1,"region": 46,"street": "Ленина","house": "1","fact_locality": 1,"fact_region": 46,"fact_street": "Ленина","fact_house": "1","birth_date": "1978-01-01","birth_place": "Москва","job": "ООО совхоз Восход","credit_sum": 1000}'>
                        Создание новой анкеты
                    </option>
                    <option value="348/">Получение существующей анкеты по ID</option>
                    <option value='348/send/{" selected_mfi_offers " : [ 2, 34, 5 ]}'>Отправка заявки в кредитную организацию</option>
                    <option value="list/?loan_type=mortgage&loan_type=cars{}">Получение списка анкет</option>
                    <option value="leads/">Получение списка заявок</option>
                    <option value="create_send_all/">Отправка заявок во все подходящие предложения</option>
                </select>
            </div>

            <div class="form-group">
                <button type="button" onclick="Send()" class="btn btn-primary pull-right">Submit</button>
            </div>
        </form>
    </div>

</div>

<div class="row" style="margin-top: 20px">
    <div id="result" class="col-md-10 col-lg-offset-1 well"></div>
</div>

</body>
</html>